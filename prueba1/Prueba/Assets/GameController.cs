﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GameController : MonoBehaviour
{
    public GameObject[] Pista;
    public GameObject[] Player;
    public Camera cam;
    private bool rotar=false;
    public int contador=0;
    private float puntero;
    private float spawn = 5;
    
  
    void Start()
    {

        Player[0] = GameObject.FindGameObjectWithTag("Square");//iniciamos el juego con el player ya instanciado en la pantalla
      


    }

    // Update is called once per frame
    void Update()
    {    
        /*cam.transform.position = new Vector3(
           Player[0].transform.position.x,
           cam.transform.position.y,
           cam.transform.position.z
           );*/

        if (Player != null && this.puntero < Player[0].transform.position.x + spawn)//Controla cuando finaliza el juego
        {
           
            GameObject newBlock = Instantiate(Pista[Random.Range(0,Pista.Length)]);
            Random.Range(0,10);
            if (Random.Range(0, 10) < 1 &&!rotar)//Si valor menor que uno se girara la camara durante un rato
            {
                
                cam.gameObject.transform.rotation = Quaternion.Euler(0, 0, 180);
                rotar = true;
                contador = 0;

            }

            if(contador<2)//controla la duración de la camara invertida
            {
                contador++;
                
            }
            else
            {

                cam.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
                
                rotar = false;
            }

            newBlock.transform.SetParent(this.transform);
            newBlock.transform.position = new Vector2(
                puntero + (63/ 2),
                -2
            );

            puntero += 30;

            

        }


    }
}
