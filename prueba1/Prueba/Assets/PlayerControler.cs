﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerControler : MonoBehaviour
{

    public int vel; //variable para la velocidad x;
    public int salt;//variable para la velocidad y;
    public int velBala;//variable para la velocidad bala;
   
    
    //variables del disparo
    public GameObject[] Shoot;
    public int disparos = 15;
    public int dcd = 15;

    //variables para controlar el salto
    public Camera cam;
    private bool salto;
    public int aire;
    private bool a=false;
    private bool walljump=false;
    public int cont = 0;
    private bool sigueContando = true;

    //variable texto
    public Text score;
    public int puntuacion = 0;
    public string punts;
    

    // Start is called before the first frame update
    void Start()
    {

       
    
       

        salto = false;//inciamos bool para controlar que solo se salte una vez

        //Shoot = GameObject.FindGameObjectsWithTag("shoot");
        score.text = "Puntos: "+punts;

     
    }

    // Update is called once per frame
    void Update()
    {
        cont++;//contador para limitar numero disparos
      
        if (Input.GetKey("d"))
        {
           this.GetComponent<Rigidbody2D>().AddForce(new Vector2(vel, 0));
           
            if (sigueContando)//si vamos hacia alante y nos desplazamos nos sumara puntos
            {
                puntuacion++;
            }
        }

        if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-vel, 0));//si vamos hacia alante y nos desplazamos nos descontara puntos
            if (sigueContando)
            {
                puntuacion--;
            }
        }

        if (Input.GetKey("w"))
        {
            
                if (!salto)//miramos si estamos en el aire, si no estamos saltaremos
                    
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, salt));
                    salto = true;

                }
            
            if (walljump && cont==aire)//cambiamos condicion de salto en el contacto con el muro y controlamos el numero de saltos en el
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, salt));
                
            }

        }

        if (Input.GetKey("s"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -vel));
        }

        if (Input.GetKey(KeyCode.Space)&&dcd==40)//controlamos el numero de disparos que puede hacer el player
        {
            dcd = 0;
        GameObject newShoot = Instantiate(Shoot[0]);
       newShoot.transform.SetParent(this.transform);

        newShoot.transform.position = new Vector2(
           this.transform.position.x +2,
           this.transform.position.y
          
       );
           
        }

        if (dcd < 40)//contador del disparo
        {
            dcd++;

        }

        cam.transform.position = new Vector3( //movemos caramara con el jugador
          this.transform.position.x+7,
           cam.transform.position.y,
           cam.transform.position.z
           );

        if (cont==aire)//contador para limitar saltos en pared
        {
            cont = 0;
            
        }

        if (this.transform.position.y <-40)//si te caes mueres, para resumir
        {
            Destroy(this.gameObject);
        }
        if (!walljump && !salto)//controlo situaciones indeseadas, aunque depues de varios cambios en el juego, no creo que haga nada. Pero prefiero prevenir
        {
            salto = false;
        }

        
       
        punts = ("" + puntuacion);
        score.text = ("Puntuación: " + punts);

        if (GetComponent<Rigidbody2D>().velocity.y > 40)//controlamos la velocidad de salto, que nunca supere 40(si saltabas desde el suelo y tocabas muro pulsando w metías un salto muy bestía)
        {
            GetComponent<Rigidbody2D>().velocity =new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 40);
        }
        {

        }
    }


  
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Murillo")//si tocas trampa mueres
        {
            Destroy(this.gameObject);
        }

        if (collision.gameObject.tag == "Suelo")//controlamos el salto, este es para el suelo
        {
            salto = false;
            walljump = false;


        }
        if(collision.gameObject.tag == "Muros")//controlamos el salto cuando estamos en el muro, aquí se desactiva el contador de puntos
        {
           
            
            salto = false;
            walljump = true;
            sigueContando = false;
        }
        

    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Muros")// controlamos salto al salir del muro
        {
            walljump = false;
            sigueContando = true;



        }
    }


}
